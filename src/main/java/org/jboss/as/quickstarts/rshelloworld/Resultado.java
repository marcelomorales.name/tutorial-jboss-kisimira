package org.jboss.as.quickstarts.rshelloworld;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * @author Marcelo Morales
 *         Since: 12/11/13
 */
@XmlRootElement(name = "xml")
public class Resultado implements Serializable {

    private String salutation;

    private String language;

    @XmlElement(name = "result")
    public String getSalutation() {
        return salutation;
    }

    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    @XmlAttribute
    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Resultado)) return false;

        Resultado resultado = (Resultado) o;

        if (language != null ? !language.equals(resultado.language) : resultado.language != null) return false;
        if (salutation != null ? !salutation.equals(resultado.salutation) : resultado.salutation != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = salutation != null ? salutation.hashCode() : 0;
        result = 31 * result + (language != null ? language.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Resultado{" +
                "salutation='" + salutation + '\'' +
                ", language='" + language + '\'' +
                '}';
    }
}
