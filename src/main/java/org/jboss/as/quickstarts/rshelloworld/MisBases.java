package org.jboss.as.quickstarts.rshelloworld;

import org.apache.commons.dbutils.QueryRunner;
import org.postgresql.ds.PGPoolingDataSource;

import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.sql.DataSource;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

/**
 * @author Marcelo Morales
 *         Since: 12/12/13
 */
@ApplicationScoped
public class MisBases {

    private static final Logger LOGGER = Logger.getLogger(MisBases.class.getName());

    private final PGPoolingDataSource ds;

    private final ExecutorService executorService;

    @Inject
    public MisBases() {
        ds = new PGPoolingDataSource();
        ds.setApplicationName("Tutorial-jboss");
        ds.setServerName("localhost");
        ds.setDatabaseName("marcelo");
        ds.setUser("marcelo");
        ds.setPassword("marcelo");
        ds.setInitialConnections(2);
        ds.setMaxConnections(50);
        LOGGER.info("Inicializando data source");

        executorService = Executors.newFixedThreadPool(2);
    }

    @Produces
    public QueryRunner queryRunner() {
        //return new QueryRunner(new InitialContext().lookup("....JNDI....."));
        return new QueryRunner(ds);
    }

    @Produces
    public Executor getEjecutor() {
        return executorService;
    }

    @Produces
    public DataSource get() {
        return ds;
    }

    @PreDestroy
    public void dest() {
        executorService.shutdown();

        LOGGER.info("Deteniendo data source");
        ds.close();
    }
}
