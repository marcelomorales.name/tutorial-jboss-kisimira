package org.jboss.as.quickstarts.rshelloworld;

import com.google.common.base.*;
import com.google.common.collect.*;
import com.google.common.io.LineReader;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.imageio.ImageIO;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.regex.Pattern;

@Path("/rest")
public class RestServices {

    private final HelloService helloService;

    private final Words words;

    public RestServices() {
        helloService = null;
        words = null;
    }

    @Inject
    public RestServices(HelloService helloService, Words words) {
        this.helloService = helloService;
        this.words = words;
    }

    @GET
    @Path("/palabrasEnDict/{phrase}")
    @Produces({"application/json"})
    public Boolean palabrasEnDict(@PathParam("phrase") String frase) {
        return words.algunaEstaEnElDicccionario(frase);
    }

    @GET
    @Path("/json")
    @Produces({"application/json"})
    public String getHelloWorldJSON() {
        return "{\"result\":\"" + helloService.createHelloMessage("World") + "\"}";
    }

    @GET
    @Path("/json2")
    @Produces({"application/json"})
    public JsonNode getHelloWorldJSON2() {
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode = objectMapper.createObjectNode();
        objectNode.put("result", helloService.createHelloMessage("Mundito"));
        return objectNode;
    }

    @GET
    @Path("/json3")
    @Produces({"application/json"})
    public Resultado getHelloWorldJSON3() {
        Resultado document = new Resultado();
        document.setLanguage("es");
        document.setSalutation("Hola Mundo Cruel!");

        return document;
    }

    @GET
    @Path("/json4")
    @Produces({"application/json"})
    public Map getHelloWorldJSON4() {
        return ImmutableMap.of("hola", "mundo", "vida", "programando");
    }

    @GET
    @Path("/xml")
    @Produces({"application/xml"})
    public String getHelloWorldXML() {
        return "<xml><result>" + helloService.createHelloMessage("World") + "</result></xml>";
    }

    @GET
    @Path("/xml2")
    @Produces({"application/xml"})
    public Document getHelloWorldXML2() throws ParserConfigurationException {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

        Document document = documentBuilder.newDocument();
        Element xml = document.createElement("xml");
        document.appendChild(xml);

        Element result = document.createElement("result");
        xml.appendChild(result);

        result.setTextContent(helloService.createHelloMessage("World"));

        return document;
    }

    @GET
    @Path("/xml3")
    @Produces({"application/xml"})
    public Resultado getHelloWorldXML3() {
        Resultado document = new Resultado();
        document.setLanguage("es");
        document.setSalutation("Hola Mundo Cruel!");

        return document;
    }

    // para probar:
    // curl -H "Accept: application/xml" -H "Content-type: text/plain" -d @archivo.txt http://localhost:8080/tutorial-jboss/rest/xml4
    @POST
    @Path("/xml4")
    @Produces({"application/xml"})
    @Consumes("text/plain")
    public Resultado getHelloWorldXML4(String texto) {
        Resultado document = new Resultado();
        document.setLanguage("es");

        Iterable<String> split =
                Splitter.on(CharMatcher.JAVA_LETTER.negate()).omitEmptyStrings().trimResults().split(texto);

        ArrayList<String> list = Lists.newArrayList(Sets.newCopyOnWriteArraySet(split));
        Collections.sort(list);

        document.setSalutation(Joiner.on("\r\n").join(list));

        return document;
    }

    private static Random RANDOM = new Random();

    @GET
    @Path("/png")
    @Produces("image/png")
    public Response generarGrafico() {
        return Response.ok().entity(new StreamingOutput() {

            @Override
            public void write(OutputStream output) throws IOException, WebApplicationException {
                BufferedImage bufferedImage = new BufferedImage(300, 300, BufferedImage.TYPE_4BYTE_ABGR);
                for (int i = 0; i < 10000; i++) {
                    bufferedImage.setRGB(RANDOM.nextInt(300), RANDOM.nextInt(300), RANDOM.nextInt());
                }
                ImageIO.write(bufferedImage, "png", output);
            }
        }).build();
    }


    @GET
    @Path("/error")
    @Produces("text/plain")
    public Response generarerror() {
        return Response.status(Response.Status.FORBIDDEN).build();
    }

    @POST
    @Path("/analize-file/{dict}")
    @Produces("application/json")
    @Consumes("text/plain")
    public Response analizar(@PathParam("dict") String dict, InputStream reader) throws IOException, ExecutionException {
        try {
            return Response.ok().entity(words.countPresent(dict, new InputStreamReader(reader, Charsets.UTF_8))).build();
        } catch (final ExecutionException e) {
            if (e.getCause() instanceof FileNotFoundException) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            throw e;
        }
    }
}
