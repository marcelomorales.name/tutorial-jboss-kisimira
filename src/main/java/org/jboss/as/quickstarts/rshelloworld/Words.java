package org.jboss.as.quickstarts.rshelloworld;

import com.google.common.base.*;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.*;
import com.google.common.io.LineReader;

import javax.inject.Singleton;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import java.util.regex.Pattern;

/**
 * @author Marcelo Morales
 *         SincLOGGER e: 12/11/13
 */
@Singleton
public class Words {

    private static final Logger LOGGER = Logger.getLogger(Words.class.getName());

    private final Predicate<String> stringPredicate;

    private final Splitter splitter;

    private final LoadingCache<String, Predicate<String>> distCache;

    public Words() throws IOException, ExecutionException {

        splitter = Splitter.on(Pattern.compile("[^a-zA-Z'ÁÉÍÓÚÑÜáéíóúñü]")).omitEmptyStrings().trimResults();

        distCache = CacheBuilder.<String, Predicate<String>>newBuilder().build(new StringPredicateCacheLoader());

        stringPredicate = distCache.get("cracklib-small");
    }

    public boolean algunaEstaEnElDicccionario(String frase) {
        if (frase == null) {
            throw new IllegalArgumentException("La frase no debe ser null");
        }
        Iterable<String> palabras =
                Splitter.on(CharMatcher.WHITESPACE).omitEmptyStrings().trimResults().split(frase.toLowerCase());
        return Iterables.any(palabras, stringPredicate);
    }

    public List<Boolean> palabrasEnDiccionario(String frase) {
        Iterable<String> palabras =
                Splitter.on(CharMatcher.WHITESPACE).omitEmptyStrings().trimResults().split(frase.toLowerCase());
        Function<String, Boolean> stringBooleanFunction = Functions.forPredicate(stringPredicate);
        Iterable<Boolean> transform = Iterables.transform(palabras, stringBooleanFunction);
        return Lists.newArrayList(transform);
    }

    public List<Boolean> palabrasEnDiccionario(String s, String dict) throws ExecutionException {
        Iterable<String> palabras = splitter.split(s.toLowerCase(new Locale("es")));
        Function<String, Boolean> stringBooleanFunction = Functions.forPredicate(distCache.get(dict));
        Iterable<Boolean> transform = Iterables.transform(palabras, stringBooleanFunction);
        return Lists.newArrayList(transform);
    }

    public Map<String, Double> countPresent(String dict, Reader reader) throws IOException, ExecutionException {
        LineReader lineReader = new LineReader(reader);
        String s;
        long count = 0;
        long presentes = 0;
        while ((s = lineReader.readLine()) != null) {
            List<Boolean> booleans = palabrasEnDiccionario(s, dict);
            count += booleans.size();
            presentes += Collections2.filter(booleans, Predicates.equalTo(true)).size();
        }

        return ImmutableMap.of(
                "presentes", (double) presentes / count,
                "ausentes", (double) (count - presentes) / count);
    }

    private static class StringPredicateCacheLoader extends CacheLoader<String, Predicate<String>> {

        @Override
        public Predicate<String> load(String key) throws Exception {
            File file = new File("/usr/share/dict/" + key);
            ImmutableSet.Builder<String> builder = new ImmutableSet.Builder<>();

            Stopwatch started = Stopwatch.createStarted();

            try (FileReader readable = new FileReader(file)) {
                LineReader lineReader = new LineReader(readable);
                String s;
                while ((s = lineReader.readLine()) != null) {
                    builder.add(s.toLowerCase(new Locale("es")));
                }
            }

            ImmutableSet<String> palabras = builder.build();

            started.stop();

            LOGGER.info("El diccionario " + key +
                    " se ha cargado en " + started.elapsed(TimeUnit.MILLISECONDS) + " milis");

            return Predicates.in(palabras);
        }
    }
}
