package org.jboss.as.quickstarts.rshelloworld;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Marcelo Morales
 *         Since: 12/12/13
 */
public class Duracion implements Serializable {

    private Long duracion;
    private Date fecha;

    public Long getDuracion() {
        return duracion;
    }

    public void setDuracion(Long duracion) {
        this.duracion = duracion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Override
    public String toString() {
        return "Duracion{" +
                "duracion=" + duracion +
                ", fecha=" + fecha +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Duracion)) return false;

        Duracion duracion1 = (Duracion) o;

        if (duracion != null ? !duracion.equals(duracion1.duracion) : duracion1.duracion != null) return false;
        if (fecha != null ? !fecha.equals(duracion1.fecha) : duracion1.fecha != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = duracion != null ? duracion.hashCode() : 0;
        result = 31 * result + (fecha != null ? fecha.hashCode() : 0);
        return result;
    }
}
