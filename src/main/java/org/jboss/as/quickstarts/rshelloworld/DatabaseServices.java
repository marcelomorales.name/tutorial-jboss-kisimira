package org.jboss.as.quickstarts.rshelloworld;

import com.google.common.base.Stopwatch;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.*;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

/**
 * @author Marcelo Morales
 *         Since: 12/12/13
 */
@Path("/db")
public class DatabaseServices {

//    @Resource(name = "java:jboss/datasources/ExampleDS")
//    private DataSource dataSource;

    private final DataSource dataSource;

    private final QueryRunner queryRunner;

    private final Executor executor;

    public DatabaseServices() {
        dataSource = null;
        queryRunner = null;
        executor = null;
    }

    @Inject
    public DatabaseServices(DataSource dataSource, QueryRunner queryRunner, Executor executor) {
        this.dataSource = dataSource;
        this.queryRunner = queryRunner;
        this.executor = executor;
    }

    @POST
    @Path("/insertduracion")
    @Consumes("application/json")
    @Produces("application/pdf")
    public Response insertar(Duracion duracion) throws SQLException {
        final int update =
                queryRunner.update("insert into duraciones values (?, ?)", duracion.getDuracion(),
                        new Timestamp(duracion.getFecha().getTime()));
        return Response.ok().entity(new StreamingOutput() {

            @Override
            public void write(OutputStream output) throws IOException, WebApplicationException {
                try {
                    final PDDocument pdDocument = new PDDocument();
                    final PDPage page = new PDPage();
                    pdDocument.addPage(page);

                    PDPageContentStream contentStream = new PDPageContentStream(pdDocument, page);
                    contentStream.beginText();
                    contentStream.setFont(PDType1Font.HELVETICA, 40);
                    contentStream.moveTextPositionByAmount( 100, 700 );
                    if (update == 1) {
                        contentStream.drawString("Small is beautiful");
                    } else {
                        contentStream.drawString("Ups. algo ha salido mal en el insert");
                    }
                    contentStream.endText();
                    contentStream.close();

                    pdDocument.save(output);

                    pdDocument.close();
                } catch (COSVisitorException e) {
                    throw new RuntimeException(e);
                }
            }
        }).build();
    }

    @GET
    @Path("/getrdp/{pag}")
    @Produces("application/json")
    public List<Map<String, Object>> sel(@PathParam("pag") int pagina) throws SQLException {
        Stopwatch stopwatch = Stopwatch.createStarted();
        List<Map<String, Object>> query = queryRunner.query("select id, duracion, fecha, url from registrodepeticion limit 5 offset ?",
                new MapListHandler(), 5 * pagina);

        final long elapsed = stopwatch.elapsed(TimeUnit.MICROSECONDS);

        executor.execute(new Runnable() {

            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                    queryRunner.update("insert into duraciones values (?, ?)", elapsed, new Date(System.currentTimeMillis()));
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        });
        return query;
    }

    @GET
    @Path("/countdbu")
    public Long countpfu() throws SQLException {
        return queryRunner.query("select count(*) from registrodepeticion", new ScalarHandler<Long>());
    }


    @GET
    @Path("/countpf")
    public Long countpf() throws SQLException {
        Connection c = dataSource.getConnection();
        try {
            Statement s = c.createStatement();
            try {
                ResultSet resultSet = s.executeQuery("select count(*) from registrodepeticion");
                try {
                    while (resultSet.next()) {
                        return resultSet.getLong(1);
                    }
                } finally {
                    resultSet.close();
                }
            } finally {
                s.close();
            }
        } finally {
            c.close();
        }
        return 0L;
    }

    @GET
    @Path("/info")
    public String info() throws NamingException, SQLException {
        DataSource dataSource =
                (DataSource) new InitialContext().lookup("java:jboss/datasources/ExampleDS");
        try (Connection c = dataSource.getConnection()) {
            DatabaseMetaData metaData = c.getMetaData();
            return "El nombre de la base es " + metaData.getDatabaseProductName();
        }
    }

    @GET
    @Path("/pginfo")
    public String infopg() throws SQLException {
        try (Connection c = dataSource.getConnection()) {
            DatabaseMetaData metaData = c.getMetaData();
            return "El nombre de la base es " + metaData.getDatabaseProductName();
        }
    }



}
