package org.jboss.as.quickstarts.rshelloworld;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import javax.ws.rs.core.Response;

import java.io.FileReader;
import java.io.Reader;

import static org.junit.Assert.assertTrue;

/**
 * @author Marcelo Morales
 *         Since: 12/11/13
 */
@RunWith(Arquillian.class)
public class RestServicesIntegrationTest {

    @Deployment
    public static JavaArchive createDeployment() {
        return ShrinkWrap.create(JavaArchive.class)
                .addClasses(Words.class, RestServices.class, HelloService.class)
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Inject
    RestServices restServices;

    @Test
    public void testWords() throws Exception {
        assertTrue(restServices.palabrasEnDict("unit"));
    }
}
