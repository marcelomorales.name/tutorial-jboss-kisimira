package org.jboss.as.quickstarts.rshelloworld;

import com.google.common.base.Stopwatch;
import org.junit.Test;
import org.slf4j.profiler.StopWatch;

import javax.ws.rs.core.Response;
import java.io.FileReader;
import java.io.Reader;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

/**
 * @author Marcelo Morales
 *         Since: 12/11/13
 */
public class WordsTest {

    @Test
    public void testAlgunaEstaEnElDicccionario() throws Exception {
        Words words = new Words();
        assertTrue(words.algunaEstaEnElDicccionario("unit testing"));
        assertTrue(words.algunaEstaEnElDicccionario("UNIT TESTING"));
        assertFalse(words.algunaEstaEnElDicccionario("UNITTESTING"));
    }

    @Test
    public void testVacio() throws Exception {
        Words words = new Words();
        assertFalse(words.algunaEstaEnElDicccionario(""));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNull() throws Exception {
        Words words = new Words();
        words.algunaEstaEnElDicccionario(null);
    }

    @Test
    public void testPalabrasALista() throws Exception {
        Words words = new Words();
        List<Boolean> booleans = words.palabrasEnDiccionario("unit testing UNITTESTING");
        assertTrue(booleans.get(0));
        assertTrue(booleans.get(1));
        assertFalse(booleans.get(2));
    }

    @Test
    public void testQuijote() throws Exception {
        Words words = new Words();
        try (Reader r = new FileReader("/home/marcelo/public_html/2000.txt.utf-8")) {
            words.countPresent("spanish", r);
        }

        Stopwatch started = Stopwatch.createStarted();
        try (Reader r = new FileReader("/home/marcelo/public_html/2000.txt.utf-8")) {
            Map<String,Double> spanish = words.countPresent("spanish", r);
            System.out.println(spanish);
        }
        started.stop();
        System.out.println(started.elapsed(TimeUnit.MILLISECONDS));
    }
}
