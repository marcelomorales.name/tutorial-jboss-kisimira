package org.jboss.as.quickstarts.rshelloworld;

import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;

/**
 * @author Marcelo Morales
 *         Since: 12/11/13
 */
public class RestServicesTest {

    @Test
    public void testHello() throws Exception {
        HelloService helloService = Mockito.mock(HelloService.class);
        Mockito.when(helloService.createHelloMessage(Mockito.anyString())).
                thenReturn("Hello Marcelo!");

        Words words = Mockito.mock(Words.class);

        RestServices restServices = new RestServices(helloService, words);

        assertEquals("{\"result\":\"Hello Marcelo!\"}", restServices.getHelloWorldJSON());
    }
}
